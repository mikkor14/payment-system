﻿using System;
using System.Collections.Generic;

namespace PaymentSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Credit cardCredit = new Credit(01, 0);
            Savings cardSavings = new Savings(02, 0);

            Note note50 = new Note(50);
            Note note100 = new Note(100);
            Note note500 = new Note(500);

            List<Currency> wallet1 = new List<Currency>();

            wallet1.Add(note50);
            wallet1.Add(note50);
            wallet1.Add(note100);
            wallet1.Add(note100);
            wallet1.Add(note100);
            wallet1.Add(note500);

            Customer Mike = new Customer("Mike", cardSavings, wallet1);
            Customer John = new Customer("Mike", cardCredit, wallet1);
            Mike.fillCard(5000);
            John.fillCard(5000);

            Product Apple = new Product("Apple", 5);
            Product Pepsi = new Product("Pepsi", 25);
            Product Steak = new Product("Steak", 300);
            Product Phone = new Product("Phone", 500);
            Product iPhone = new Product("iPhone", 10000);

            //Simulate a purchase with a savings card

            //Expected output 5000
            Console.WriteLine(Mike.card.Balance);
            Mike.buyItemWithCard(Steak);

            //Expected output 4700
            Console.WriteLine(Mike.card.Balance);

            //Expected output not allowed to buy
            Mike.buyItemWithCard(iPhone);

            //Expected output a warning and -5000 balance
            John.buyItemWithCard(iPhone);
            Console.WriteLine(John.card.Balance);

            //Simulate a purchase with cash

            //Expected output 900
            Console.WriteLine(Mike.walletBalance);

            Mike.buyItemWithCash(Steak);

            //Expected output 600
            Console.WriteLine(Mike.walletBalance);
            Mike.printWalletNotes();

            Mike.buyItemWithCash(Phone);

            //Expected output 100
            Console.WriteLine(Mike.walletBalance);
            Mike.printWalletNotes();


        }
    }
}
