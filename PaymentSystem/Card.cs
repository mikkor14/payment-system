﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentSystem
{
    abstract class Card
    {
        private int number;
        private int balance;

        public int Balance { get => balance; set => balance = value; }
        public int Number { get => number; set => number = value; }

        public Card(int number, int balance)
        {
            this.number = number;
            this.balance = balance;
        }

        public abstract int Withdraw(int ammount);

        public abstract int AddFunds(int ammount);
    }
}
