﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentSystem
{
    class Currency
    {
        private int value;

        public Currency(int value)
        {
            this.value = value;
        }

        public int Value { get => value; set => this.value = value; }
    }
}
