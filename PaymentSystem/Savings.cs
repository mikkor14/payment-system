﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentSystem
{
    class Savings : Card
    {

        public Savings(int balance, int ammount) : base(balance, ammount)
        {

        }

        public override int Withdraw(int ammount)
        {

            if (Balance - ammount >= 0)
            {
                Balance -= ammount;
    
            } else
            {
                Console.WriteLine("You do not have enough money on your account to purchase this product");
            }
            return Balance;
        }

        public override int AddFunds(int ammount)
        {
            return Balance += ammount;
        }
    }
}
