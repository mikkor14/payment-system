﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PaymentSystem
{
    class Customer
    {
        public String name;
        public Card card;
        public List<Currency> wallet;
        public int walletBalance;

        public Customer(String name, Card card, List<Currency> wallet)
        {
            this.name = name;
            this.card = card;
            this.wallet = wallet;
            foreach (Currency money in wallet)
            {
                walletBalance += money.Value;
            }
        }

        public void buyItemWithCard(Product product)
        {
            card.Withdraw(product.Price);
        }

        public void fillCard(int ammount)
        {
            card.AddFunds(ammount);
        }


        public void updateWalletBalance()
        {
            walletBalance = 0;
            foreach (Currency money in wallet)
            {
                walletBalance += money.Value;
            }
        }

        //This one is rather heavy so i will try to explain it with some comments
        public void buyItemWithCash(Product product)
        {
            int price = product.Price;
            List<int> walletList = new List<int>();
            bool paidFull = false;
            bool isChange = false;

            //To use the notes in the wallet we need to translate it to a collection that is easy to calculate on
            foreach (Currency money in wallet)
            {
                walletList.Add(money.Value);
            }

            /*            //The algorithm finds the closes note to the price of the product and removes it from the walletList.
                        The walletList and the remaining money are then updated*/

            while (!paidFull)
            {
                int closest = walletList.Aggregate((x, y) => Math.Abs(x - price) < Math.Abs(y - price) ? x : y);
                price -= closest;
                walletList.Remove(closest);
                //If the remaining ammount you have to pay reaches 0, you are done paying
                if (price == 0)
                {
                    paidFull = true;
                }
                else if (price < 0)  //If you overpayed then you gotta get your change back
                {
                    price = price * -1;
                    paidFull = true;
                    isChange = true;
                }

            }

            wallet.Clear();

            //For the time being the only change the customer can recieve is 50 notes. 
            if (isChange)
            {
                while (price > 0)
                {
                    wallet.Add(new Note(50));
                    price -= 50;
                }
            }

            //Fills the wallet with the remaining notes represented as ints in walletList
            foreach (int money in walletList)
            {
                wallet.Add(new Note(money));
            }

            //Helper method to update the balance value
            updateWalletBalance();


        }


        public void printWalletNotes()
        {
            foreach (Currency note in wallet)
            {
                Console.WriteLine($"$$${note.Value}$$$");
            }
        }

    }
}
