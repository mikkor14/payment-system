﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentSystem
{
    class Credit : Card
    {

        public Credit(int number, int balance) : base(number, balance)
        {

        }

        public override int Withdraw(int ammount)
        {

            Balance -= ammount;
            if(Balance < 0)
            {
                Console.WriteLine("You have overcharged your account, you can still continute shopping, but be carefull with your future spending");
            }
            return Balance;
        }

        public override int AddFunds(int ammount)
        {
            return Balance += ammount;
        }


    }
}
