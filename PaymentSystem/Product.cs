﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentSystem
{
    class Product
    {
        String name;
        int price;

        public Product(String name, int price)
        {
            this.Name = name;
            this.Price = price;
        }

        public int Price { get => price; set => price = value; }
        public string Name { get => name; set => name = value; }
    }
}
